<?php

namespace Tests\ProductPlugin\WordPressPlugin;

use ProductPlugin\WordPressPlugin\Hook;
use ProductPlugin\WordPressPlugin\Hooks_Loader;

class Hooks_Loader_Test extends \PHPUnit_Framework_TestCase {
    /**
     * @var Hooks_Loader
     */
    private $hooks_loader;

    public function setUp() {
        \WP_Mock::setUp();

        $this->hooks_loader = new Hooks_Loader();
    }

    public function tearDown() {
        \WP_Mock::tearDown();
		\Mockery::close();
    }

    /**
     * @test
     */
    public function it_adds_action() {
		$hook_tag = 'test_hook';
		$object = new \stdClass();
		$callback_method = 'method_name';

		$priority = 33;
		$accepted_args = 2;

    	$hook = new Hook(
			$hook_tag,
			$object,
			$callback_method,
			$priority,
			$accepted_args
		);

    	$hookable = \Mockery::mock('Hookable');
		$hookable->shouldReceive('get_hooks')->andReturn(array($hook));

		$this->hooks_loader->add_hookable($hookable);

        \WP_Mock::expectActionAdded( $hook_tag, array($object, 'method_name'), $priority, $accepted_args );
        $this->hooks_loader->run();
    }
}