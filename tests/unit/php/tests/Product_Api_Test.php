<?php

namespace Tests\ProductPlugin;

use ProductPlugin\Product_Api;
use ProductPlugin\Repository\Product_Repository;

class Product_Api_Test extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var Product_Api
	 */
	private $product_api;

	/**
	 * @var Product_Repository
	 */
	private $product_repository;

	public function setUp()
	{
		\WP_Mock::setUp();
		$this->product_repository = \Mockery::mock('ProductPlugin\Repository\Product_Repository');
		$this->product_api = new Product_Api($this->product_repository);
	}

	public function tearDown() {
		\WP_Mock::tearDown();
		\Mockery::close();
	}

	/**
	 * @test
	 */
	public function it_returns_product_response()
	{
		$request = array('id' => 1);
		$url = 'https://url.com';

		$post = new \stdClass;
		$post->ID = 1;

		\WP_Mock::wpFunction( 'get_post', array(
			'times' => 1,
			'args' => array( $post->ID ),
			'return' => $post,
		) );

		$product = \Mockery::mock('ProductPlugin\Model\Product');
		$product
			->shouldReceive('to_array')
			->once()
			->andReturn(array('url' => $url));

		$this->product_repository
			->shouldReceive('get')
			->with($post)
			->once()
			->andReturn($product);

		$product_response = $this->product_api->get_product($request);
		$this->assertEquals($url, $product_response['url']);
	}

	/**
	 * @test
	 */
	public function it_adds_product()
	{
		$url = 'https://url.com';

		$request = array(
			'id' => 1,
			'post_title' => 'title',
			'post_content' => 'content',
			'url' => $url
		);

		$post = new \stdClass;
		$post->ID = 1;

		\WP_Mock::wpFunction( 'wp_insert_post', array(
			'times' => 1,
			'args' => array( \WP_Mock\Functions::type( 'array' )  ),
			'return' => 2,
		) );

		\WP_Mock::wpFunction( 'sanitize_title', array(
			'times' => 1,
			'args' => array( \WP_Mock\Functions::type( 'string' ) ),
			'return' => 'title',
		) );

		\WP_Mock::wpFunction( 'sanitize_url', array(
			'times' => 1,
			'args' => array( $url ),
			'return' => $url,
		) );

		\WP_Mock::wpFunction( 'sanitize_text_field', array(
			'times' => 1,
			'args' => array( \WP_Mock\Functions::type( 'string' ) ),
			'return' => 'content',
		) );

		$this->product_repository
			->shouldReceive('save_custom_data')
			->with(\Mockery::type('ProductPlugin\Model\Product'))
			->once()
			->andReturn($post->ID);

		$product_response = $this->product_api->add_product($request);
		$this->assertEquals($url, $product_response['url']);
	}

	/**
	 * @test
	 */
	public function it_updates_product()
	{
		$url = 'https://url.com';

		$request = array(
			'id' => 1,
			'post_title' => 'title',
			'post_content' => 'content',
			'url' => $url
		);

		$post = new \stdClass;
		$post->ID = 1;

		\WP_Mock::wpFunction( 'get_post', array(
			'times' => 2,
			'args' => array( $post->ID ),
			'return' => $post,
		) );

		\WP_Mock::wpFunction( 'wp_insert_post', array(
			'times' => 1,
			'args' => array( \WP_Mock\Functions::type( 'array' )  ),
			'return' => $post->ID,
		) );

		\WP_Mock::wpFunction( 'sanitize_title', array(
			'times' => 1,
			'args' => array( \WP_Mock\Functions::type( 'string' ) ),
			'return' => 'title',
		) );

		\WP_Mock::wpFunction( 'sanitize_url', array(
			'times' => 1,
			'args' => array( $url ),
			'return' => $url,
		) );

		\WP_Mock::wpFunction( 'sanitize_text_field', array(
			'times' => 1,
			'args' => array( \WP_Mock\Functions::type( 'string' ) ),
			'return' => 'content',
		) );

		$product = \Mockery::mock('ProductPlugin\Model\Product');

		$this->product_repository
			->shouldReceive('get')
			->with($post)
			->once()
			->andReturn($product);

		$this->product_repository
			->shouldReceive('save_custom_data')
			->with(\Mockery::type('ProductPlugin\Model\Product'))
			->once()
			->andReturn($post->ID);

		$product_response = $this->product_api->update_product($request);
		$this->assertEquals($url, $product_response['url']);
	}


	/**
	 * @test
	 */
	public function it_deletes_product_response()
	{
		$request = array('id' => 1);

		$post = new \stdClass;
		$post->ID = 1;

		\WP_Mock::wpFunction( 'get_post', array(
			'times' => 1,
			'args' => array( $post->ID ),
			'return' => $post,
		) );

		\WP_Mock::wpFunction( 'wp_delete_post', array(
			'times' => 1,
			'args' => array( $post->ID ),
			'return' => $post,
		) );


		$product_response = $this->product_api->delete_product($request);
		$this->assertEquals('Product delated.', $product_response['message']);
	}
}