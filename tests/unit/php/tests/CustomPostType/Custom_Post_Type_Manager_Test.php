<?php
namespace Tests\ProductPlugin\CustomPostType;

use ProductPlugin\CustomPostType\Custom_Post_Type_Manager;
use ProductPlugin\WordPressPlugin\Hook;

class Custom_Post_Type_Manager_Test extends \PHPUnit_Framework_TestCase {
	/**
	 * @var Custom_Post_Type_Manager
	 */
	private $custom_post_type_manager;

	public function setUp() {

		$this->custom_post_type_manager = new Custom_Post_Type_Manager();
	}

	public function tearDown() {
		\Mockery::close();
	}

	/**
	 * @test
	 */
	public function it_returns_custom_post_hooks() {
		$custom_post_type1 = \Mockery::mock('ProductPlugin\CustomPostType\Custom_Post_Type');
		$custom_post_type2 = \Mockery::mock('ProductPlugin\CustomPostType\Custom_Post_Type');

		$this->custom_post_type_manager->add_custom_post_type( $custom_post_type1 );
		$this->custom_post_type_manager->add_custom_post_type( $custom_post_type2 );

		$result = $this->custom_post_type_manager->get_hooks();
		$this->assertCount( 6, $result );
		$this->assertTrue( $this->check_result_hooks( $result, $custom_post_type1 ) );
		$this->assertTrue( $this->check_result_hooks( $result, $custom_post_type2 ) );
	}

	/**
	 * @param Hook[] $result
	 * @param $expected_custom_post_type
	 */
	private function check_result_hooks($result, $expected_custom_post_type) {
		$expectedTags = array(
			'init' => 'register_custom_post_type',
			'admin_init' => 'add_custom_fields',
			'save_post' => 'save'
		);

		foreach ($expectedTags as $expectedTag => $expectedMethod) {
			if ($this->is_hook_present( $result, $expectedTag, $expectedMethod, $expected_custom_post_type )) {
				break;
			}

			return false;
		}

		return true;
	}

	private function is_hook_present($result, $expectedTag, $expectedMethod, $expected_custom_post_type) {
		foreach ($result as $hook) {
			if ($hook->get_hook_tag() === $expectedTag
				&& $hook->get_object() === $expected_custom_post_type
				&& $hook->get_method() === $expectedMethod) {
				return true;
			}
		}

		return false;
	}
}