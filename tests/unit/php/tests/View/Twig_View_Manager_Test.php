<?php

namespace Tests\ProductPlugin\View;

use ProductPlugin\View\Twig_View_Manager;

class Twig_View_Manager_Test extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var Twig_View_Manager
	 */
	private $twig_view_manager;

	/**
	 * @var \Twig_Environment
	 */
	private $twig_engine;

	public function setUp()
	{
		$this->twig_engine = \Mockery::mock('\Twig_Environment');
		$this->twig_view_manager = new Twig_View_Manager($this->twig_engine);
	}

	public function tearDown() {
		\Mockery::close();
	}

	/**
	 * @test
	 */
	public function it_returns_rendered_view()
	{
		$view_name = 'test_view';
		$parameters = array('test_parameter' => 'test_value');

		$expected_view_filename = sprintf('%s.twig', $view_name);

		$rendered_twig = 'expected rendered twig';

		$this->twig_engine
			->shouldReceive('render')
			->with($expected_view_filename, $parameters)
			->once()
			->andReturn($rendered_twig);

		$result = $this->twig_view_manager->render($view_name, $parameters);
		$this->assertEquals($rendered_twig, $result);
	}
}