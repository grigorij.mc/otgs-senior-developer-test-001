<?php
namespace Tests\ProductPlugin\Model;

use ProductPlugin\Model\Product_Factory;

class Product_Factory_Test extends \PHPUnit_Framework_TestCase {
	/**
	 * @test
	 */
	public function it_creates_product() {
		$post = new \stdClass;

		$url = 'https://url.com';

		$result = Product_Factory::create( $post, $url );

		$this->assertInstanceOf('ProductPlugin\Model\Product', $result);
		$this->assertEquals( $url, $result->get_url() );
		$this->assertEquals( $post, $result->get_post() );
	}
}