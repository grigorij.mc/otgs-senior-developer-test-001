<?php

namespace Tests\ProductPlugin;

use ProductPlugin\CustomPostType\Custom_Post_Type_Manager;
use ProductPlugin\Product_Api;
use ProductPlugin\Product_Plugin;
use ProductPlugin\Repository\Product_Repository;
use ProductPlugin\View\Twig_View_Manager;
use ProductPlugin\WordPressPlugin\Hooks_Loader;

class Product_Plugin_Test extends \PHPUnit_Framework_TestCase
{
	/**
	 * @var Product_Plugin
	 */
	private $product_plugin;

	/**
	 * @var Hooks_Loader
	 */
	private $hooks_loader;

	/**
	 * @var Twig_View_Manager
	 */
	private $view_manager;

	/**
	 * @var Product_Repository
	 */
	private $product_repository;

	/**
	 * @var Product_Api
	 */
	private $product_api;

	/**
	 * @var Custom_Post_Type_Manager
	 */
	private $custom_post_type_manager;

	public function setUp()
	{
		$this->hooks_loader = \Mockery::mock('ProductPlugin\WordPressPlugin\Hooks_Loader');
		$this->view_manager = \Mockery::mock('ProductPlugin\View\Twig_View_Manager');
		$this->product_repository = \Mockery::mock('ProductPlugin\Repository\Product_Repository');
		$this->product_api = \Mockery::mock('ProductPlugin\Product_Api');
		$this->custom_post_type_manager = \Mockery::mock('ProductPlugin\CustomPostType\Custom_Post_Type_Manager');

		$this->product_plugin = new Product_Plugin(
			$this->hooks_loader,
			$this->view_manager,
			$this->product_repository,
			$this->product_api,
			$this->custom_post_type_manager
		);
	}

	public function tearDown() {
		\Mockery::close();
	}

	/**
	 * @test
	 */
	public function it_runs_plugin()
	{
		$this->custom_post_type_manager
			->shouldReceive('add_custom_post_type')
			->with(\Mockery::type('ProductPlugin\CustomPostType\Product_Post_Type'))
			->once();

		$this->hooks_loader
			->shouldReceive('add_hookable')
			->with($this->product_api)
			->once();

		$this->hooks_loader
			->shouldReceive('add_hookable')
			->with($this->custom_post_type_manager)
			->once();

		$this->hooks_loader
			->shouldReceive('run')
			->once();

		$this->product_plugin->run();
	}
}