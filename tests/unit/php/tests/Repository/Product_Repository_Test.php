<?php

namespace Tests\ProductPlugin\Repository;

use ProductPlugin\CustomPostType\Product_Post_Type;
use ProductPlugin\Model\Product;
use ProductPlugin\Repository\Product_Repository;
use \WP_Mock;

class Product_Repository_Test extends \PHPUnit_Framework_TestCase {
    /**
     * @var Product_Repository
     */
    private $product_repository;

    public function setUp() {
        \WP_Mock::setUp();

        $this->product_repository = new Product_Repository();
    }

    public function tearDown() {
        \WP_Mock::tearDown();
		\Mockery::close();
    }

    /**
     * @test
     */
    public function it_gets_product() {
		$post = new \stdClass;
		$post->ID = 1;

		$url = 'https://url.com';

		\WP_Mock::wpFunction( 'get_post_type', array(
			'times' => 1,
			'args' => array( WP_Mock\Functions::type('\stdClass') ),
			'return' => Product_Post_Type::POST_TYPE,
		) );

		\WP_Mock::wpFunction( 'get_post_meta', array(
			'times' => 1,
			'args' => array( $post->ID, Product::URL_META_KEY, true ),
			'return' => $url,
		) );

		$result = $this->product_repository->get( $post );
		$this->assertInstanceOf('ProductPlugin\Model\Product', $result);
		$this->assertEquals( $url, $result->get_url() );
    }

	/**
	 * @test
	 */
	public function it_doesnt_gets_product_if_type_not_fit() {
		$post = new \stdClass;

		\WP_Mock::wpFunction( 'get_post_type', array(
			'times' => 1,
			'args' => array( WP_Mock\Functions::type('\stdClass') ),
			'return' => 'other_type',
		) );


		$result = $this->product_repository->get( $post );
		$this->assertNull( $result );
	}

	/**
	 * @test
	 */
	public function it_saves_custom_data() {
		$post = new \stdClass;
		$post->ID = 1;

		$url = 'https://url.com';

		$product = new Product($post);
		$product->set_url($url);

		\WP_Mock::wpFunction( 'update_post_meta', array(
			'times' => 1,
			'args' => array( $post->ID, Product::URL_META_KEY, $url ),
			'return' => 'other_type',
		) );

		$this->product_repository->save_custom_data( $product );
	}
}