<?php
/*
Plugin Name: OTGS Senior Developer Test 001
Description: See `README.md`
Author: Grzegorz Wierzba
Author URI: http://www.onthegosystems.com/
Version: 0.0.1
Plugin Slug: otgs-senior-developer-test-001
*/

define( 'OTGS_SDT001_PATH', dirname( __FILE__ ) );
define( 'OTGS_SDT001_URL', untrailingslashit( plugin_dir_url( __FILE__ ) ) );

include 'vendor/autoload.php';

use ProductPlugin\Product_Plugin;
use ProductPlugin\WordPressPlugin\Hooks_Loader;
use ProductPlugin\View\Twig_View_Manager_Factory;
use ProductPlugin\Repository\Product_Repository;
use ProductPlugin\Product_Api;
use ProductPlugin\CustomPostType\Custom_Post_Type_Manager;

function run_plugin() {
	$product_repository = new Product_Repository();

	$plugin = new Product_Plugin(
		new Hooks_Loader(),
		Twig_View_Manager_Factory::create(),
		$product_repository,
		new Product_Api($product_repository),
		new Custom_Post_Type_Manager()
	);

	$plugin->run();
}

run_plugin();
