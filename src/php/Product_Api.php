<?php

namespace ProductPlugin;

use ProductPlugin\CustomPostType\Product_Post_Type;
use ProductPlugin\Model\Product_Factory;
use ProductPlugin\Repository\Product_Repository;
use ProductPlugin\WordPressPlugin\Hook;
use ProductPlugin\WordPressPlugin\Hookable;

class Product_Api implements Hookable {
	const API_NAMESPACE = '/otgs/SDT001/v1';
	const GET_PRODUCT_ROUTE = '/product/(?P<id>\d+)';
	const ADD_PRODUCT_ROUTE = '/product';

	/**
	 * @var Product_Repository
	 */
	private $product_repository;

	/**
	 * @param Product_Repository $product_repository
	 */
	public function __construct( Product_Repository $product_repository ) {
		$this->product_repository = $product_repository;
	}

	/**
	 * @param array $request
	 * @return array|\WP_Error
	 */
	public function get_product( $request ) {
		$productId = (int) $request['id'];
		$post = get_post( $productId );
		$product = $this->product_repository->get( $post );

		if ( is_null( $product ) ) {
			return $this->handle_product_not_found();
		}

		return $product->to_array();
	}

	/**
	 * @param array $request
	 * @return array|\WP_Error
	 */
	public function add_product( $request ) {
		$post_params = $this->prepare_post_params( $request );

		$postId = wp_insert_post( $post_params );

		if ( $postId === 0 ) {
			return new \WP_Error(
				'adding_error',
				'Product adding error',
				array( 'status' => 404 )
			);
		}

		if ( is_null( $request['url'] ) ) {
			return new \WP_Error(
				'url_missing',
				'Product url missing',
				array( 'status' => 404 )
			);
		}

		$url = sanitize_url( $request['url'] );
		$product = Product_Factory::create( get_post( $postId ), $url );

		$this->product_repository->save_custom_data( $product );

		return $product->to_array();
	}

	/**
	 * @param array $request
	 * @return array|\WP_Error
	 */
	public function update_product( $request ) {
		$productId = (int) $request['id'];
		$post = get_post( $productId );
		$product = $this->product_repository->get( $post );

		if ( is_null( $product ) ) {
			return $this->handle_product_not_found();
		}

		$post_params = $this->prepare_post_params( $request );

		$postId = wp_insert_post( $post_params );

		if ( $postId === 0 ) {
			return new \WP_Error(
				'adding_error',
				'Product adding error',
				array( 'status' => 404 )
			);
		}

		if ( is_null($request['url'] ) ) {
			return new \WP_Error(
				'url_missing',
				'Product url missing',
				array( 'status' => 404 )
			);
		}

		$url = sanitize_url( $request['url'] );
		$product = Product_Factory::create( get_post( $postId ), $url );

		$this->product_repository->save_custom_data( $product );

		return $product->to_array();
	}

	/**
	 * @param array $request
	 * @return array|\WP_Error
	 */
	public function delete_product( $request ) {
		$productId = (int) $request['id'];
		$post = get_post( $productId );

		$result = wp_delete_post( $post->ID );

		if ( is_null( $result ) ) {
			return new \WP_Error(
				'product_not_deleted',
				'Product can not be deleted',
				array( 'status' => 404 )
			);
		}

		return array('message' => 'Product delated.');
	}

	/**
	 * @return \WP_Error
	 */
	private function handle_product_not_found( ) {
		return new \WP_Error(
			'product_not_found',
			'Product not found',
			array( 'status' => 404 )
		);
	}

	/**
	 * @param $request
	 * @return array
	 */
	private function prepare_post_params( $request ) {
		$post_params['post_type'] = Product_Post_Type::POST_TYPE;
		$post_params['post_status'] = 'publish';
		$post_params['comment_status'] = 'closed';
		$post_params['ping_status'] = 'closed';
		$post_params['post_title'] = sanitize_title( $request['post_title'] );
		$post_params['post_content'] = sanitize_text_field( $request['post_content'] );
		$post_params['ping_status'] = 'closed';

		if ( isset( $request['id'] ) ) {
			$post_params['ID'] = (int) $request['id'];
		}

		return $post_params;
	}

	public function register_api_routes() {
		register_rest_route(
			Product_Api::API_NAMESPACE,
			Product_Api::GET_PRODUCT_ROUTE,
			array(
				'methods' => 'GET',
				'callback' => array( $this, 'get_product' )
			)
		);

		register_rest_route(
			Product_Api::API_NAMESPACE,
			Product_Api::ADD_PRODUCT_ROUTE,
			array(
				'methods' => 'POST',
				'callback' => array( $this, 'add_product' ),
			)
		);

		register_rest_route(
			Product_Api::API_NAMESPACE,
			Product_Api::GET_PRODUCT_ROUTE,
			array(
				'methods' => 'PUT',
				'callback' => array( $this, 'update_product' )
			)
		);

		register_rest_route(
			Product_Api::API_NAMESPACE,
			Product_Api::GET_PRODUCT_ROUTE,
			array(
				'methods' => 'DELETE',
				'callback' => array( $this, 'delete_product' )
			)
		);
	}

	/**
	 * @return array
	 */
	public function get_hooks() {
		return array(
			new Hook( 'rest_api_init', $this, 'register_api_routes' )
		);
	}
}
