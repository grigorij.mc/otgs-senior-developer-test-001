<?php
namespace ProductPlugin\CustomPostType;

use ProductPlugin\WordPressPlugin\Hook;
use ProductPlugin\WordPressPlugin\Hookable;

class Custom_Post_Type_Manager implements Hookable {
	/**
	 * @var array
	 */
	private $custom_post_types;

	public function __construct() {
		$this->custom_post_types = array();
	}

	/**
	 * @param Custom_Post_Type $custom_post_type
	 */
	public function add_custom_post_type( Custom_Post_Type $custom_post_type ) {
		$this->custom_post_types[] = $custom_post_type;
	}

	/**
	 * @return array
	 */
	public function get_hooks() {
		$hooks = array();

		foreach ($this->custom_post_types as  $custom_post_type) {
			$hooks[] = new Hook( 'init', $custom_post_type, 'register_custom_post_type' );
			$hooks[] = new Hook( 'admin_init', $custom_post_type, 'add_custom_fields' );
			$hooks[] = new Hook( 'save_post', $custom_post_type, 'save' );
		}

		return $hooks;
	}
}