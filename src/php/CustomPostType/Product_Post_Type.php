<?php

namespace ProductPlugin\CustomPostType;

class Product_Post_Type extends Custom_Post_Type {
	const POST_TYPE = 'product';
	const POST_TYPE_NAME = 'Products';
	const POST_TYPE_SINGULAR_NAME = 'Product';

	public function add_custom_fields() {
		add_meta_box(
			static::POST_TYPE . '_custom_fields-meta',
			static::POST_TYPE_SINGULAR_NAME,
			array( $this, 'custom_fields' ),
			self::POST_TYPE
		);
	}

	public function custom_fields() {
		global $post;
		$product = $this->repository->get( $post );

		$url = $product->get_url();

		echo $this->view_manager->render(
			'custom_post_type_product_meta_fields',
			array('url' => $url)
		);
	}

	public function save() {
		if(is_admin()) {
			global $post;
			$product = $this->repository->get( $post );
			$product->set_url( esc_url($_POST['url']) );

			$this->repository->save_custom_data( $product );
		}
	}
}