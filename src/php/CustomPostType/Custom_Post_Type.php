<?php

namespace ProductPlugin\CustomPostType;

use ProductPlugin\Repository\Repository;
use ProductPlugin\View\View_Manager;

abstract class Custom_Post_Type {
	const POST_TYPE = 'POST_TYPE';
	const POST_TYPE_NAME = 'POST_TYPE_NAME';
	const POST_TYPE_SINGULAR_NAME = 'POST_TYPE_SINGULAR_NAME';

	/**
	 * @var View_Manager
	 */
	protected $view_manager;

	/**
	 * @var Repository
	 */
	protected $repository;

	public abstract function add_custom_fields();

	public abstract function save();

	/**
	 * @param View_Manager $view_manager
	 * @param Repository $repository
	 */
	public function __construct( View_Manager $view_manager, Repository $repository ) {
		$this->view_manager = $view_manager;
		$this->repository = $repository;
	}

	public function register_custom_post_type() {
		register_post_type(
			static::POST_TYPE,
			$this->get_arguments()
		);
	}

	/**
	 * @return array
	 */
	private function get_arguments() {
		return array(
			'labels' => array(
				'name' => __( static::POST_TYPE_NAME ),
				'singular_name' => __( static::POST_TYPE_SINGULAR_NAME ),
			),
			'public' => true,
			'has_archive' => true,
		);
	}
}