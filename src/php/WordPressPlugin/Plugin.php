<?php

namespace ProductPlugin\WordPressPlugin;

use ProductPlugin\View\Twig_View_Manager;
use ProductPlugin\View\View_Manager;

abstract class Plugin {
	/**
	 * @var Hooks_Loader
	 */
	protected $loader;

	/**
	 * @var View_Manager
	 */
	protected $view_manager;

	/**
	 * @param Hooks_Loader $hooks_loader
	 * @param Twig_View_Manager $view_manager
	 */
	public function __construct( Hooks_Loader $hooks_loader, Twig_View_Manager $view_manager ) {
		$this->loader = $hooks_loader;
		$this->view_manager = $view_manager;
	}

	public function run() {
		$this->loader->run();
	}
}
