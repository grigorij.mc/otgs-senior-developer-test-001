<?php

namespace ProductPlugin\WordPressPlugin;

class Hooks_Loader {
	/**
	 * @var array
	 */
	private $hookable_collection;

	public function __construct() {
		$this->hookable_collection = array();
	}

	/**
	 * @param Hookable $hookable
	 */
	public function add_hookable( $hookable ) {
		$this->hookable_collection[] = $hookable;
	}

	public function run() {
		foreach ( $this->hookable_collection as $hookable ) {
			$hooks = $hookable->get_hooks();
			$this->add_actions( $hooks );
		}
	}

	/**
	 * @param Hook[] $hooks
	 */
	private function add_actions( $hooks ) {
		foreach ( $hooks as $hook ) {
			add_action(
				$hook->get_hook_tag(),
				array(
					$hook->get_object(),
					$hook->get_method()
				),
				$hook->get_priority(),
				$hook->get_accepted_args()
			);
		}
	}
}
