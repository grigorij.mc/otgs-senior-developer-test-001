<?php

namespace ProductPlugin\WordPressPlugin;


class Hook {
	/**
	 * @var string
	 */
	private $hook_tag;

	/**
	 * @var Hookable
	 */
	private $object;

	/**
	 * @var string
	 */
	private $method;

	/**
	 * @var int
	 */
	private $priority;

	/**
	 * @var int
	 */
	private $accepted_args;

	/**
	 * @param string $hook_tag
	 * @param $object
	 * @param string $method
	 * @param int $priority
	 * @param int $accepted_args
	 */
	public function __construct( $hook_tag, $object, $method, $priority = 10, $accepted_args = 1 ) {
		$this->hook_tag = $hook_tag;
		$this->object = $object;
		$this->method = $method;
		$this->priority = $priority;
		$this->accepted_args = $accepted_args;
	}

	/**
	 * @return string
	 */
	public function get_hook_tag() {
		return $this->hook_tag;
	}

	/**
	 * @return Hookable
	 */
	public function get_object() {
		return $this->object;
	}

	/**
	 * @return string
	 */
	public function get_method() {
		return $this->method;
	}

	/**
	 * @return int
	 */
	public function get_priority() {
		return $this->priority;
	}

	/**
	 * @return int
	 */
	public function get_accepted_args() {
		return $this->accepted_args;
	}
}