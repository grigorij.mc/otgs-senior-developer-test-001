<?php

namespace ProductPlugin\WordPressPlugin;

interface Hookable {
	/**
	 * @return array
	 */
	public function get_hooks();
}