<?php

namespace ProductPlugin\Model;

class Product_Factory
{
	/**
	 * @param $post
	 * @param string $url
	 * @return Product
	 */
	public static function create( $post, $url ) {
		$product = new Product( $post );
		$product->set_url( $url );

		return $product;
	}
}