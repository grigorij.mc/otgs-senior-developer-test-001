<?php

namespace ProductPlugin\Model;

abstract class Base_Model {
	const URL_META_KEY = 'URL_META_KEY';

	protected $post;

	/**
	 * @param $post
	 */
	public function __construct( $post ) {
		$this->post = $post;
	}

	/**
	 * @return mixed
	 */
	public function get_post() {
		return $this->post;
	}

	/**
	 * @return array
	 */
	public function to_array() {
		return array( 'post' => $this->post );
	}
}