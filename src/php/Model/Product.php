<?php

namespace ProductPlugin\Model;

class Product extends Base_Model {
	const URL_META_KEY = 'product_url';

	/**
	 * @var string
	 */
	private $url;

	/**
	 * @param $post
	 */
	public function __construct( $post ) {
		parent::__construct( $post );
	}

	/**
	 * @param string $url
	 */
	public function set_url( $url ) {
		$this->url = $url;
	}

	/**
	 * @return string
	 */
	public function get_url() {
		return $this->url;
	}

	/**
	 * @return array
	 */
	public function to_array() {
		return array(
			'post' => $this->post,
			'url' => $this->url
		);
	}
}