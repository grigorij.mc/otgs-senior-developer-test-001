<?php

namespace ProductPlugin\Repository;

use ProductPlugin\Model\Base_Model;

interface Repository {
	/**
	 * @param $post
	 * @return Base_Model
	 */
	public function get( $post );

	/**
	 * @param Base_Model $model
	 */
	public function save_custom_data( Base_Model $model );
}