<?php

namespace ProductPlugin\Repository;

use ProductPlugin\CustomPostType\Product_Post_Type;
use ProductPlugin\Model\Base_Model;
use ProductPlugin\Model\Product;
use ProductPlugin\Model\Product_Factory;

class Product_Repository implements Repository {
	/**
	 * @param $post
	 * @return null|Product
	 */
	public function get( $post ) {
		if ( $post !== null && get_post_type( $post ) === Product_Post_Type::POST_TYPE ) {
			$url = get_post_meta( $post->ID, Product::URL_META_KEY, true );

			return Product_Factory::create( $post, $url );
		}

		return null;
	}

	/**
	 * @param Base_Model $product
	 */
	public function save_custom_data( Base_Model $product ) {
		update_post_meta( $product->get_post()->ID, Product::URL_META_KEY, $product->get_url() );
	}
}