<?php

namespace ProductPlugin\View;

use \Twig_Environment;

class Twig_View_Manager implements View_Manager {
	/**
	 * @var Twig_Environment
	 */
	private $twig;

	/**
	 * @param Twig_Environment $twig
	 */
	public function __construct( Twig_Environment $twig ) {
		$this->twig = $twig;
	}

	/**
	 * @param string $view_name
	 * @param array $parameters
	 * @return string
	 */
	public function render( $view_name, $parameters = array() ) {
		return $this->twig->render(
			sprintf('%s.twig', $view_name),
			$parameters
		);
	}
}