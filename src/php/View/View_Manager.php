<?php
namespace ProductPlugin\View;

interface View_Manager {
	/**
	 * @param string $view
	 * @param array $parameters
	 * @return string
	 */
	public function render( $view, $parameters = array() );
}