<?php

namespace ProductPlugin\View;

use \Twig_Environment;
use \Twig_Loader_Filesystem;

class Twig_View_Manager_Factory {
	public static function create() {
		$template_loader = new Twig_Loader_Filesystem( OTGS_SDT001_PATH . '/src/templates' );
		$twig = new Twig_Environment( $template_loader );

		return new Twig_View_Manager( $twig );
	}
}