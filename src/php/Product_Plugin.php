<?php

namespace ProductPlugin;

use ProductPlugin\CustomPostType\Custom_Post_Type_Manager;
use ProductPlugin\CustomPostType\Product_Post_Type;
use ProductPlugin\Repository\Product_Repository;
use ProductPlugin\View\Twig_View_Manager;
use ProductPlugin\WordPressPlugin\Hooks_Loader;
use ProductPlugin\WordPressPlugin\Plugin;

class Product_Plugin extends Plugin {
	/**
	 * @var Product_Api
	 */
	private $products_api_endpoint;

	/**
	 * @var Product_Repository
	 */
	private $product_repository;

	/**
	 * @var Custom_Post_Type_Manager
	 */
	private $custom_post_type_manager;

	/**
	 * @param Hooks_Loader $hooks_loader
	 * @param Twig_View_Manager $view_manager
	 * @param Product_Repository $product_repository
	 * @param Product_Api $products_api_endpoint
	 * @param Custom_Post_Type_Manager $custom_post_type_manager
	 */
	public function __construct(
		Hooks_Loader $hooks_loader,
		Twig_View_Manager $view_manager,
		Product_Repository $product_repository,
		Product_Api $products_api_endpoint,
		Custom_Post_Type_Manager $custom_post_type_manager
	) {
		parent::__construct( $hooks_loader, $view_manager );

		$this->product_repository = $product_repository;
		$this->products_api_endpoint = $products_api_endpoint;
		$this->custom_post_type_manager = $custom_post_type_manager;
	}

	public function run() {
		$this->add_post_types();

		$this->loader->add_hookable( $this->products_api_endpoint );
		$this->loader->add_hookable( $this->custom_post_type_manager );

		$this->loader->run();
	}

	private function add_post_types() {
		$this->custom_post_type_manager->add_custom_post_type(
			new Product_Post_Type(
				$this->view_manager,
				$this->product_repository
			)
		);
	}
}